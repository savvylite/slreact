import LinkOut from './assets/img/LinkOut'

const MenuItem = ({ item }) => {
    const getItem = (item) => {
        if (item.href) {
            const e = item.href
            return (
                <a href={e[1]} {...(e[2] && { target: '_blank' })}>
                    <span>{e[0]}</span>
                    {e[2] && <LinkOut />}
                </a>
            )
        } else {
            return (
                /* TODO ... provisoire, voir d'autres cas */
                <a href="null" alt="">
                    {item.label}
                </a>
            )
        }
    }
    return getItem(item)
}

export default MenuItem
