import React, {
    useState,
    useRef,
    useEffect,
    forwardRef,
    useImperativeHandle,
    useContext,
} from 'react'

import './BaseSLComponents.css'

import { PopupContext } from './customHooks'

const Popup = forwardRef((props, ref) => {
    const [popups, setPopups] = useContext(PopupContext).popups
    const [currentDocClickHandler, setCurrentDocClickHandler] =
        useContext(PopupContext).currentDocumentClickHandler
    const [nbOpenedPopups, setNbOpenedPopups] =
        useContext(PopupContext).openedPopups
    const [_isOpen, setOpen] = useState(false)

    useEffect(() => {
        setPopups((popups) => [...popups, ref])
    }, [ref, setPopups])

    useEffect(() => {
        if (_isOpen) popupRef.current.classList.add('open')
        else popupRef.current.classList.remove('open')
        if (props.stateChanged) props.stateChanged(_isOpen)
        // eslint-disable-next-line
    }, [_isOpen, ref])

    const onClickOutsidePopup = (e) => {
        if (e.target.closest('.sl-popup-00') === null) {
            // console.log('click outside...')
            closeAllPopupsBut(null)
        }
    }

    /*
     * Ferme tous le popups hormi celui passé en paramètre 'except'
     * Si except === null tous le popups sont fermés
     * Retourne le nombre de popups qui ont été fermés
     */
    const closeAllPopupsBut = (except) => {
        let count = 0
        popups.forEach((p) => {
            if (
                p !== except &&
                !p.current.shouldKeepOpen() &&
                p.current.isOpen()
            ) {
                p.current.close()
                count++
            }
        })
        return count
    }

    const popupRef = useRef(null)

    const shouldKeepOpen = () => props.keepopen

    const classname = props.className
        ? `sl-popup-00 ${props.className}`
        : `sl-popup-00`

    const toggle = (ev) => {
        ev.stopPropagation()
        const nbOpened = nbOpenedPopups - closeAllPopupsBut(ref)
        if (_isOpen) close()
        else {
            if (nbOpened === 0) {
                // premier popup à s'ouvrir
                setCurrentDocClickHandler((handler) => onClickOutsidePopup)
                document.addEventListener('click', onClickOutsidePopup)
            }
            setNbOpenedPopups(nbOpened + 1)
            setOpen(true)
        }
    }

    const close = () => {
        setOpen(false)
        if (nbOpenedPopups === 1) {
            // dernier popup à se fermer
            document.removeEventListener('click', currentDocClickHandler)
        }
        setNbOpenedPopups(nbOpenedPopups - 1)
    }

    const isOpen = () => _isOpen

    useImperativeHandle(ref, () => ({
        toggle,
        isOpen,
        shouldKeepOpen,
        close,
    }))

    return (
        <div ref={popupRef} className="sl-popup-00-wrapper">
            <div className="sl-popup-00-absolute-wrapper">
                <div ref={ref} className={classname}>
                    {props.children}
                </div>
            </div>
        </div>
    )
})

export default Popup
