import React, { useRef } from 'react'
import './BaseSLComponents.css'
import MenuItem from './MenuItem'
import Popup from './Popup'
import ArrowDown from './assets/img/ArrowDown'

const Menu = ({ menu }) => {
    const popup = useRef(null)
    const wrapper = useRef(null)

    const onStateChanged = (open) => {
        if (open) wrapper.current.classList.add('open')
        else wrapper.current.classList.remove('open')
    }

    return (
        <div ref={wrapper} className="sl-menu-00-wrapper">
            <div onClick={(ev) => popup.current.toggle(ev)}>
                <span>{menu.label}</span>
                <span>
                    <ArrowDown />
                </span>
            </div>
            <Popup stateChanged={onStateChanged} ref={popup}>
                <div className="sl-menu-00">
                    {menu.items.map((item, idx) => (
                        <MenuItem key={idx} item={item} />
                    ))}
                </div>
            </Popup>
        </div>
    )
}

export default Menu
