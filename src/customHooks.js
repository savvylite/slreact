import { createContext, useState } from 'react'

export const LanguageContext = createContext()

export const useLan = () => {
    const [language, setLanguage] = useState('fr')
    return { language, setLanguage }
}

export const PopupContext = createContext()

export const usePopups = () => {
    const [popups, setPopups] = useState([])
    const [nbOpenedPopups, setNbOpenedPopups] = useState(0)
    const [currentDocClickHandler, setCurrentDocClickHandler] = useState(null)

    return {
        popups: [popups, setPopups],
        openedPopups: [nbOpenedPopups, setNbOpenedPopups],
        currentDocumentClickHandler: [
            currentDocClickHandler,
            setCurrentDocClickHandler,
        ],
    }
}
