const enDic = new Map()
const frDic = new Map()
const ruDic = new Map()

const dictionnary = new Map()
dictionnary['en'] = enDic
dictionnary['fr'] = frDic
dictionnary['ru'] = ruDic

enDic['psw'] = 'Password'
frDic['psw'] = 'Mot de passe'
ruDic['psw'] = 'Пароль'

enDic['email'] = 'Email address'
frDic['email'] = 'Adresse email'
ruDic['email'] = 'Адрес почты'

enDic['firstname'] = 'First name'
frDic['firstname'] = 'Prénom'
ruDic['firstname'] = 'Имя'

enDic['lastname'] = 'Last name'
frDic['lastname'] = 'Nom'
ruDic['lastname'] = 'Фамилия'

enDic['mailerror'] = 'Invalid email format'
frDic['mailerror'] = "Le format de l'adresse email n'est pas valide."
ruDic['mailerror'] = 'Неверный формат электроннной почты.'

enDic['charserror'] = (a, b) => `${a} to ${b} characters expected.`
frDic['charserror'] = (a, b) => `Entre ${a} et ${b} caractères attendus.`
ruDic['charserror'] = (a, b) => `Ожидается от ${a} до ${b} символов.`

enDic['pswerror'] = 'The password must contain:'
frDic['pswerror'] = 'Le mot de passe doit contenir:'
ruDic['pswerror'] = 'Пароль должен содержать:'

enDic['pswerrdetail'] = 'At least 8 charactères.'
frDic['pswerrdetail'] = 'Au moins 8 caractères.'
ruDic['pswerrdetail'] = 'Не менее 8 символов.'

enDic['tippsw1'] = 'Show'
frDic['tippsw1'] = 'Afficher le mot'
ruDic['tippsw1'] = 'Показать'
enDic['tippsw2'] = 'Hide'
frDic['tippsw2'] = 'Cacher'
ruDic['tippsw2'] = 'Скрыть'
enDic['tippsw'] = 'password'
frDic['tippsw'] = 'de passe'
ruDic['tippsw'] = 'пароль'

enDic['cnxbtn'] = 'Sign in'
frDic['cnxbtn'] = 'Se connecter'
ruDic['cnxbtn'] = 'Войти'

enDic['ctnbtn'] = 'Continue'
frDic['ctnbtn'] = 'Continuer'
ruDic['ctnbtn'] = 'Продолжить'

export default dictionnary
