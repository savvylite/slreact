import './BaseSLComponents.css'
import { useContext } from 'react'
import { LanguageContext } from './customHooks'

const SLButton = ({ className, label }) => {
    const lanContext = useContext(LanguageContext)
    const lan = lanContext ? lanContext.language : 'fr'

    return (
        <button className="form-button blue" type="submit">
            {typeof label === 'string' ? label : label[lan]}
        </button>
    )
}

export default SLButton
