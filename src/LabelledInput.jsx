import React, { useEffect, useState, useContext } from 'react'
import './Labelled-input.css'
import './tooltip.css'
import Eye from './assets/img/Eye'
import EyeClosed from './assets/img/EyeClosed'
import { LanguageContext } from './customHooks'

const LabelledInput = ({
    id,
    name,
    type,
    value,
    label /* "mon label" ou {fr: "mon label fr", en:"my label en", ...} */,
    className,
    error /* le message d'erreur associé au champ */,
    novalidation /* aucune validation sur le champ */,
    validation /*  valeurs possibles : text-1-50 : entre 1 et 50 caractères, todo : prendre en charge d'autres valeurs */,
    /* onFocus,*/
}) => {
    const lanContext = useContext(LanguageContext)
    const lan = lanContext ? lanContext.language : 'fr'

    const ref = React.useRef()
    const [visible, setVisible] = useState(
        type === 'password' ? false : undefined
    )
    const [pswOK, setPswOK] = useState(type === 'password' ? false : undefined)

    useEffect(() => {
        ref.current.validate = validate
        // eslint-disable-next-line
    }, [])

    const labelEmail = {
        en: 'Email address',
        fr: 'Adresse email',
        ru: 'Адрес почты',
    }
    const labelPassword = { en: 'Password', fr: 'Mot de passe', ru: 'Пароль' }
    const labelFirstname = { en: 'First name', fr: 'Prénom', ru: 'Имя' }
    const labelLastname = { en: 'Last name', fr: 'Nom', ru: 'Фамилия' }
    const mailError = {
        en: 'Invalid email format',
        fr: "Le format de l'adresse email n'est pas valide.",
        ru: 'Неверный формат электроннной почты.',
    }

    const pswError = {
        en: 'The password must contain:',
        fr: 'Le mot de passe doit contenir:',
        ru: 'Пароль должен содержать:',
    }

    const pswErrDetail = {
        en: 'At least 8 charactères.',
        fr: 'Au moins 8 caractères.',
        ru: 'Не менее 8 символов.',
    }

    const charsError = {
        en: (a, b) => `${a} to ${b} characters expected.`,
        fr: (a, b) => `Entre ${a} et ${b} caractères attendus.`,
        ru: (a, b) => `Ожидается от ${a} до ${b} символов.`,
    }

    const tipPsw1 = {
        en: 'Show',
        fr: 'Afficher le mot',
        ru: 'Показать',
    }
    const tipPsw2 = {
        en: 'Hide',
        fr: 'Cacher le mot',
        ru: 'Скрыть',
    }
    const tipPsw = {
        en: 'password',
        fr: 'de passe',
        ru: 'пароль',
    }

    className = className
        ? className
        : type === 'password'
        ? 'lbld-password'
        : ''
    let inputType = undefined

    if (!type) type = 'text'
    if (type.startsWith('text-')) {
        inputType = 'text'
    }

    switch (type) {
        case 'email':
            inputType = 'text'
            break
        default:
            inputType = type
    }

    if (!label) {
        if (type === 'email') label = labelEmail[lan]
        else if (type === 'password') label = labelPassword[lan]
        else if (type === 'firstname') label = labelFirstname[lan]
        else if (type === 'lastname') label = labelLastname[lan]
    } else {
        if (typeof label === 'object') label = label[lan]
    }

    if (!name) {
        if (type === 'email') name = 'email'
        else if (type === 'password') name = 'password'
        else if (type === 'firstname') name = 'firstname'
        else if (type === 'lastname') name = 'lastname'
    }

    if (!validation) {
        if (type === 'firstname' || type === 'lastname')
            validation = 'text-1-50'
    }

    if (!error) {
        if (type === 'email') error = mailError[lan]
        if (validation && validation.startsWith('text-')) {
            const tab = validation.split('-')
            error = charsError[lan](tab[1], tab[2])
        }
    }

    const handleBlur = (ev) => {
        if (ev.target.value === '')
            ev.target.nextSibling.classList.remove('labellized')
        ev.target.nextSibling.classList.remove('focused')
    }

    const handleFocus = (ev) => {
        ev.target.nextSibling.classList.add('labellized', 'focused')
        const lbld = ref.current.closest('.labelled-input')
        if (lbld.classList.contains('error') && ref.current.value === '')
            lbld.classList.remove('error')
        /* if (onFocus) onFocus(ev)*/
    }

    const showError = () => {
        const lbld = ref.current.closest('.labelled-input')
        lbld.classList.add('error')
        return null
    }

    const validate = () => {
        const value = ref.current.value
        if (novalidation) return { [name]: ref.current.value }
        if (type === 'email') {
            const regex_mail = new RegExp(
                // eslint-disable-next-line
                /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/g
            )
            const matched = value.match(regex_mail)
            if (matched && matched[0] === value) {
                let usermail = matched[0].trim()
                return { [name]: usermail }
            } else return showError()
        } else if (validation && validation.startsWith('text-')) {
            const tab = validation.split('-')
            if (value.length >= tab[1] && value.length <= tab[2]) {
                return { [name]: value }
            } else return showError()
        } else if (type === 'password') {
            // A MODIFIER
            if (value.length > 8) {
                return { [name]: ref.current.value }
            } else {
                ref.current.parentNode.nextSibling.classList.add('error')
                return showError()
            }
        } else {
            return { [name]: ref.current.value }
        }
    }

    const clearError = (ev) => {
        if (novalidation) return
        const lbld = ref.current.closest('.labelled-input')
        if (lbld.classList.contains('error')) lbld.classList.remove('error')
        const v = ev.currentTarget.value
        if (type === 'password') {
            if (v.length >= 8) setPswOK(true)
            else setPswOK(false)
            const wrapper = ref.current.parentNode.nextSibling
            if (wrapper.classList.contains('error'))
                wrapper.classList.remove('error')
            if (v.length > 0) {
                wrapper.classList.add('activate')
            } else {
                wrapper.classList.remove('activate')
            }
        }
    }

    const showComponent = () => {
        if (visible === undefined) {
            return (
                // différent d'un champ password
                <div>
                    <input
                        ref={ref}
                        className="lbld-input"
                        {...(id && { id })}
                        {...(name && { name })}
                        {...(inputType && { type: inputType })}
                        {...(value && { value })}
                        spellCheck="false"
                        onBlur={handleBlur}
                        onFocus={handleFocus}
                        autoComplete="username"
                        onChange={clearError}
                    />
                    <div className="lbld-label">{label}</div>
                </div>
            )
        } else {
            // champ password
            return (
                <div>
                    <input
                        ref={ref}
                        className={`lbld-input ${
                            type === 'password' && 'password'
                        }`}
                        {...(id && { id })}
                        {...(name && { name })}
                        type={visible ? 'text' : 'password'}
                        {...(value && { value })}
                        spellCheck="false"
                        onBlur={handleBlur}
                        onFocus={handleFocus}
                        autoComplete="username"
                        onChange={clearError}
                    />
                    <>
                        <div className="lbld-label">{label}</div>
                        <div
                            onClick={() => {
                                setVisible((visible) => !visible)
                            }}
                            className={
                                visible ? 'lbld-eye visible' : 'lbld-eye hidden'
                            }
                        >
                            {visible ? <Eye /> : <EyeClosed />}

                            <div className="tooltip">
                                {visible ? tipPsw2[lan] : tipPsw1[lan]}
                                <br /> {tipPsw[lan]}
                            </div>
                        </div>
                    </>
                </div>
            )
        }
    }

    return (
        <div className={`labelled-input ${className}`}>
            {showComponent()}
            {type !== 'password' && (
                <div className="lbld-invalid-input">
                    <span></span>
                    {error}
                </div>
            )}
            {type === 'password' && (
                <div className="lbld-password-info-wrapper">
                    <div className="lbld-password-info">
                        <span>{pswError[lan]}</span>
                        <div {...(pswOK && { className: 'lbld-pswOK' })}>
                            {pswOK ? <span>✓</span> : <span>•</span>}{' '}
                            {pswErrDetail[lan]}
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default LabelledInput
