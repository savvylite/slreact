import './testPopup.css'
import { PopupContext, usePopups } from './customHooks'
import { LanguageContext, useLan } from './customHooks'
import Menu from './Menu'
import Select from './Select'
import FlagFr from './tests/FlagFr'
import FlagEn from './tests/FlagEn'
import FlagRu from './tests/FlagRu'
import LabelledInput from './LabelledInput'
import SLButton from './SLButton'
import Form from './Form'

function App() {
    const popupsContextValue = usePopups()
    const lanContextValue = useLan()

    const menu1 = {
        label: 'Administration',
        items: [
            {
                href: ['Mathématiques', ' https://google.fr', true],
            },
            { label: 'Second item' },
        ],
    }

    /* const menu2 = {
        label: 'Thèmes',
        items: [
            {
                href: ['Data science', ' https://google.fr'],
            },
            { label: 'React' },
        ],
    }*/

    //  const options = ['rouge', 'vert foncé kaki', 'bleu', 'jaune']
    const options = [
        ['Français', 'fr', FlagFr],
        ['English', 'en', FlagEn],
        ['Русский', 'ru', FlagRu],
    ]

    const handleLoginSubmit = (formData) => {
        console.log(formData)
    }

    return (
        <LanguageContext.Provider value={lanContextValue}>
            <PopupContext.Provider value={popupsContextValue}>
                <div
                    style={{
                        margin: '50px',
                        display: 'flex',
                        alignItems: 'baseline',
                        gap: '25px',
                    }}
                >
                    <p>
                        Il était une fois une fille de roi au coeur plein de
                        tristesse.
                    </p>

                    <Menu menu={menu1} />
                    {/* <Menu menu={menu2} />*/}
                    <Select
                        selected={0}
                        onSelect={(item, id) => {
                            //console.log(id, ' : ', item)
                            lanContextValue.setLanguage(item)
                        }}
                        options={options}
                        className="myselect"
                    />
                </div>
                <p>
                    Il était une fois une fille de roi au coeur plein de
                    tristesse.
                </p>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        width: '300px',
                        marginLeft: '50px',
                    }}
                >
                    <Form onSubmit={handleLoginSubmit}>
                        <LabelledInput
                            label={{
                                en: 'Sign in',
                                fr: 'Se connecter',
                                ru: 'Войти',
                            }}
                        />
                        <LabelledInput type="firstname" />
                        <LabelledInput type="lastname" />
                        <LabelledInput type="email" />
                        <LabelledInput type="password" />
                        <Select
                            selected={2}
                            name="lan"
                            onSelect={(item, id) => {
                                console.log(
                                    id,
                                    ' : ',
                                    item,
                                    '     provisoire, testing....'
                                )
                            }}
                            options={options}
                            className="myselect"
                        />
                        <SLButton
                            label={{
                                en: 'Sign in',
                                fr: 'Se connecter',
                                ru: 'Войти',
                            }}
                            // lan={lanContextValue.language}
                        />
                    </Form>
                </div>
            </PopupContext.Provider>
        </LanguageContext.Provider>
    )
}

export default App
