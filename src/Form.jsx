import React from 'react'

/*
 * props : className - optionnel
 * onSubmit : obligatoire : est appelée lorsque tous les champs sont validés
 *                          (s'ils disposent d'une fonction validate)
 *                          reçoit alors en paramètre l'objet formData dont les champs ont pour attribut
 *                          name ou data-name et leur valeur value correspondante
 */
export default function Form({ className, onSubmit, children }) {
    const classname = className ? `sl-select ${className}` : 'sl-select'

    const handleFormSubmit = (ev) => {
        ev.preventDefault()
        const fields = ev.target.querySelectorAll('[name]')
        let formData = {}
        let isFormValidated = true
        for (let e of fields) {
            if (e.validate) {
                let good = e.validate()
                if (good) formData = { ...formData, ...e.validate() }
                else isFormValidated = false
            } else {
                // pas de fonction de validation associée au champ
                //let name = e.name || e['data-name']
                if (e.name) {
                    formData = {
                        ...formData,
                        ...{ [e.name]: e.value },
                    }
                }
            }
        }
        const dataFields = ev.target.querySelectorAll('div[data-name]')
        for (let e of dataFields) {
            if (e.validate) {
                let good = e.validate()
                if (good) formData = { ...formData, ...e.validate() }
                else isFormValidated = false
            } else {
                // pas de fonction de validation associée au champ
                formData = {
                    ...formData,
                    ...{
                        [e.getAttribute('data-name')]:
                            e.getAttribute('data-value'),
                    },
                }
            }
        }
        if (isFormValidated) onSubmit(formData)
    }

    return (
        <form className={classname} onSubmit={handleFormSubmit}>
            {children}
        </form>
    )
}
