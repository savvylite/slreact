const FlagFr = () => {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 15 10"
            width="15"
            height="10"
        >
            <rect width="15" height="10" fill="#CE1126" />
            <rect width="10" height="10" fill="#FFFFFF" />
            <rect width="5" height="10" fill="#002654" />
        </svg>
    )
}

export default FlagFr
