const FlagRu = () => {
    return (
        <svg
            id="Layer_1"
            data-name="Layer 1"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 15 10"
            width="15"
            height="10"
        >
            <rect fill="#fff" width="15" height="5" />
            <rect fill="#d52b1e" y="5" width="15" height="5" />
            <rect fill="#0039a6" y="3.3" width="15" height="3.3" />
        </svg>
    )
}

export default FlagRu
